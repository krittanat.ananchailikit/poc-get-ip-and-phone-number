package com.example.myapplication

import android.Manifest
import android.content.Context
import android.content.Context.TELEPHONY_SUBSCRIPTION_SERVICE
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.myapplication.databinding.FragmentSecondBinding
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException
import java.util.*


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = requireNotNull(_binding)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }
        binding.textviewSecond.text = getMyPhoneNumber()
    }

    private fun getMyPhoneAddress(): String {
        // host + port
        var proxyAddress = ""
        try {
            val en: Enumeration<NetworkInterface> = NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val intf: NetworkInterface = en.nextElement()
                val enumIpAddr: Enumeration<InetAddress> = intf.inetAddresses
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress: InetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress
                        && !inetAddress.isAnyLocalAddress
                        && !inetAddress.isLinkLocalAddress
                        && !inetAddress.isMulticastAddress
                        && inetAddress.isSiteLocalAddress
                        && inetAddress is Inet4Address
                    ) {
                        proxyAddress += inetAddress.hostAddress + "\r\n"
                    }
                }
            }
        } catch (ex: SocketException) {
            Log.e("", "", ex)
        }
        return proxyAddress
    }

    private fun getMyPhoneNumber(): String {
        // phone
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_PHONE_NUMBERS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            // phone
            val subscriptionManager =
                requireContext().getSystemService(TELEPHONY_SUBSCRIPTION_SERVICE) as SubscriptionManager
            val subsInfoList = subscriptionManager.activeSubscriptionInfoList
            var numbers = "Phone number"
            Log.d("Test", "Current list = $subsInfoList")
            for (subscriptionInfo in subsInfoList) {
                val number = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    subscriptionManager.getPhoneNumber(subscriptionInfo.subscriptionId) + "get from android 13 or more"
                } else {
                    subscriptionInfo.number
                }

                // host + port
                Log.d("Test", " Number is  $number")
                var proxyAddress = "host:"+System.getProperty("http.proxyHost");
                proxyAddress += "port:" + System.getProperty("http.proxyPort");
                numbers += "$number  $proxyAddress"
            }
            return numbers
        }
        return ""
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}